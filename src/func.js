const getSum = (str1, str2) => {
  if (typeof str1 !== 'string' || typeof str2 !== 'string') {
    return false;
  } else {
    if (str1.length === 0) {
      str1 = '0';
    }
    if (str2.length === 0) {
      str2 = '0';
    }
    let isNum1 = /^\d+$/.test(str1);
    let isNum2 = /^\d+$/.test(str2);
    if (isNum1 && isNum2) {
      str1 = parseInt(str1);
      str2 = parseInt(str2);
      return `${str1 + str2}`;
    } else {
      return false;
    }
  }
};

const getQuantityPostsByAuthor = (listOfPosts, authorName) => {
  const posts = listOfPosts.filter((post) => post.author === authorName);
  const comments = listOfPosts.reduce((quantity, post) => {
    if (post.hasOwnProperty('comments')) {
      let counter = 0;
      post.comments.forEach((comment) => {
        if (comment.author === authorName) {
          counter += 1;
        }
      });
      return quantity + counter;
    } else {
      return quantity;
    }
  }, 0);
  return `Post:${posts.length},comments:${comments}`;
};

const tickets = (people) => {
  let initValue = 0;
  if (Array.isArray(people)) {
    const results = people.map((person) => {
      const money = parseInt(person);
      if (isNaN(money)) {
        return 'NO';
      } else {
        if (money === 25) {
          initValue += 25;
          return 'YES';
        } else if (money > 25 && money - 25 > initValue) {
          return 'NO';
        } else {
          initValue += money - 25;
          return 'YES';
        }
      }
    });
    return results.every((e) => e === 'YES') ? 'YES' : 'NO';
  } else {
    return 'NO';
  }
};

module.exports = { getSum, getQuantityPostsByAuthor, tickets };
